<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

/*
|--------------------------------------------------------------------------
| Web Post Routes
|--------------------------------------------------------------------------
|
*/

// Route::get('/posts', 'PostController');

/*
|--------------------------------------------------------------------------
| Built-in Authentication Routes
|--------------------------------------------------------------------------
|
*/
Auth::routes();
Route::get('login/google', 'Auth\GoogleLoginController@redirectToProvider');

Route::get('login/google/callback', 'Auth\GoogleLoginController@handleProviderCallback');

/*
|--------------------------------------------------------------------------
| Authenticated Only Routes
|--------------------------------------------------------------------------
|
| These routes uses the auth middleware, therefore they are only
| accessible to authenticated users.
|
*/
Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    /*
    |--------------------------------------------------------------------------
    | Profile Routes
    |--------------------------------------------------------------------------
    |
    */
    Route::get('user/{param}', 'ProfileController@index')->name('profile.index');
    Route::put('user/profile/{user}', 'ProfileController@update')->name('profile.update');
    Route::put('user/password/{user}', 'ProfileController@passwordUpdate')->name('password.update');

    /*
    |--------------------------------------------------------------------------
    | Image uploads
    |--------------------------------------------------------------------------
    |
    */
    Route::post('image', 'ImageController@store')->name('image.create');

    /*
    |--------------------------------------------------------------------------
    | PREFIX : admin
    |--------------------------------------------------------------------------
    |
    | These routes are prefixed with 'admin'
    |
    */
    Route::prefix('admin')->group(function () {
        Route::get('/user/spy/back', 'UsersController@spyBack')->name('users.spy.back');
        Route::get('/user/spy/{id}', 'UsersController@spy')->name('users.spy');

        /*
        |--------------------------------------------------------------------------
        | Blog
        |--------------------------------------------------------------------------
        |
        */
        Route::resource('/post', 'PostController');

        /*
        |--------------------------------------------------------------------------
        | Permissions
        |--------------------------------------------------------------------------
        |
        */
        Route::resource('/permissions', 'PermissionsController')->except('update', 'edit');
        Route::post('/permissions/add', 'PermissionsController@add')->name('permissions.add');
        Route::post('/permissions/revoke', 'PermissionsController@revoke')->name('permissions.revoke');

        /*
        |--------------------------------------------------------------------------
        | Permissions
        |--------------------------------------------------------------------------
        |
        */
        Route::resource('/roles', 'RolesController');

        /*
        |--------------------------------------------------------------------------
        | Users Permissions
        |--------------------------------------------------------------------------
        |
        */
        Route::get('/users', 'UsersController@index')->name('users.index');
        Route::post('/user/api/{user}/add', 'UsersController@createUserToken')->name('users.api.add');
        Route::get('/user/api/{usertoken}/delete', 'UsersController@deleteUserToken')->name('users.api.delete');
        Route::get('/users/{id}/add', 'UsersController@add')->name('users.add');
        Route::get('/users/{id}/remove', 'UsersController@remove')->name('users.remove');
    });
});
