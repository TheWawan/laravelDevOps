<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\User;
use App\UserToken;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($param)
    {
        $user = User::find(auth()->id());

        $token = JWTAuth::fromUser($user);

        $users = User::all();

        return view('user.index', compact('user', 'users', 'param', 'token'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, User $user)
    {
        $user->update($request->only(['name', 'email']));

        return back();
    }

    public function passwordUpdate(PasswordRequest $request, User $user)
    {
        $user->password = bcrypt($request->password);

        $user->save();

        return back();
    }
}
