<?php

namespace App\Http\Controllers;

use App\User;
use App\UserToken;
use Auth;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * Show the users roles dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function spy($id)
    {
        if (!session()->get('ghostUser')) {
            $ghostUser = Auth::user();

            session(['ghostUser' => $ghostUser]);
        }

        Auth::loginUsingId($id);


        return redirect()->back();
    }

    public function spyBack()
    {
        $user = session('ghostUser');

        session()->forget('ghostUser');

        Auth::login($user);

        return back();
    }
}
