<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermissionForm;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsController extends Controller
{
    /**
     * Display Listing ressources
     * @return Redirect
     */
    public function index()
    {
        $permissions = Permission::all();

        return view('permission.index', compact('permissions'));
    }

    /**
     * Create permission
     * @param  PermissionForm $form
     * @return Redirect
     */
    public function store(PermissionForm $form)
    {
        $form->persist();

        return back();
    }


    /**
     * Destroy a permission
     * @param  Permission $permission
     * @return Redirect
     */
    public function destroy(Permission $permission)
    {
        $permission->delete();

        return back();
    }

    /**
     * Add permission to Role
     * @param Role $role
     */
    public function add(Role $role)
    {
        $role::find(request('role'))->givePermissionTo(request('permission'));

        return back();
    }

    /**
     * Revoke a permission
     * @param  Role   $role
     * @return Redirect
     */
    public function revoke(Role $role)
    {
        $role = $role::find(request('role'));

        $permission = Permission::find(request('permission'));

        $role->revokePermissionTo($permission);

        return back();
    }
}
