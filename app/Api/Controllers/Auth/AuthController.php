<?php

namespace App\Api\Controllers\Auth;

use App\Api\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{

    /**
     * Get credentials and create a unique token for the user
     * @param  Request $request Api Request
     * @return Json Response        Bearer Token
     */
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    /**
     * Get the user for the requested token
     * @return Response User $user
     */
    public function getUser()
    {
        $user = JWTAuth::parseToken()->toUser();

        return Response()->json(compact('user'));
    }
}
