<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'google_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relationship: A user writes many posts.
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function publish(Post $post)
    {
        $this->posts()->save($post);
    }

    public static function saveGoogleToken($user, $googleUser)
    {
        $user->fill([
            'google_token' => $googleUser->token,
        ])->save();
    }

    public static function createGoogleUser($googleUser)
    {
        $user = User::create([
                'name'          => $googleUser->name,
                'email'         => $googleUser->email,
                'google_token'  => $googleUser->token
        ]);
    }
}
