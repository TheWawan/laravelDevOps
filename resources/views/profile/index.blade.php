@extends('layouts.app')

@section('content')
<div class="px-2 nunito">
  <div class="flex -mx-2">
    @include('layouts.partials.profile.menu')
    @include('layouts.partials.profile.content')
  </div>
</div>
@endsection()
