<div class="w-2/3 px-2 rounded">
	<div class="rounded border-2 border-grey-light mb-8 ">
		<div class="h-12 border-grey-dark">
			<p class="p-4">Personnal Token</p>
		</div>
			<div class="h-auto w-auto border-t-2 text-grey-dark ">
				<div class="bg-white shadow-lg rounded-lg overflow-hidden p-4 m-4 ">
					<div class="sm:flex sm:items-center px-6 py-4">
						<div class="sm:text-center sm:flex-grow w-full">
								<small class="text-xm " style="hyphens: auto;">{{$token}}</small>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
