<div class="w-2/3 px-2 rounded">
	<div class="rounded border-2 border-grey-light mb-8">
		<div class="h-12 border-grey-dark">
			<p class="p-4">Sécurity</p>
		</div>
		<div class="h-auto border-t-2 text-grey-dark">
			<form method="POST" action="{{ route('password.update', ['user' => $user->id]) }}" class="mb-4 text-center">
				{{ method_field('PUT')}}
				{{ csrf_field()}}
				<div class="inline-block relative w-1/3 mr-auto ml-auto">
					<label class="inline-block relative w-1/3 mr-auto ml-auto" for="password">Password</label>
				    <input class="bg-white appearance-none border-2 border-grey-light hover:border-blue rounded w-full py-2 px-4 text-grey-darker mr-4" id="blog-title" type="password" name="password" value="{{ old('password') }}">
				</div>
				<div class="inline-block relative w-1/3">
					<label class="inline-block relative w-1/3" for="password_confirmation">Password Confirm</label>
				    <input class="bg-white appearance-none border-2 border-grey-light hover:border-blue rounded w-full py-2 px-4 text-grey-darker ml-4" id="blog-title" type="password" name="password_confirmation"}}">
				</div>
				<button class="bg-blue text-white p-2 m-4 rounded">Send</button>
			</form>
		</div>
	</div>
</div>
