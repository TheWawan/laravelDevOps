@extends('layouts.app')

@section('content')
<div class="px-2 nunito">
  <div class="flex -mx-2">
	@include('layouts.partials.user.menu')
	@include('layouts.partials.user.content')
  </div>
</div>
@endsection()
